<?php

namespace App\Http\Controllers\Core;

// Control Base
use App\Http\Controllers\Controller as BaseController;

// Traits
use App\Traits\ApiResponse;
use App\Traits\RestControllerTrait;

// Request
use Illuminate\Http\Request;

// Modelos
use App\User;

/**
 * Controlador de usuarios
 *
 * @category Controller
 * @package  App\Http\Controllers\Core
 * @author   Alejandro Méndez <alejmendez.87@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.html MIT
 * @link     http://url.com
 */
class UserController extends BaseController
{
    use RestControllerTrait, ApiResponse;

    const MODEL = 'App\User';
    const PERMISSION = 'user';

    protected $validationRules = [
        'email'    => 'required',
        'name'     => 'required',
        'role'     => 'required',
        'password' => 'required'
    ];

    /**
     * User: devuelve el usuario autenticado
     *
     * @param Request $request Request de la socilitud
     *
     * @return App\User
     */
    public function user(Request $request)
    {
        return $request->user();
    }

    protected function getAll()
    {
        $m = self::MODEL;
        return $m::select('id', 'name', 'email')->with('roles:name');
    }

    public function getOne($id)
    {
        $m = self::MODEL;
        return $m::select('id', 'name', 'email', 'verified')
            ->with('roles:id,name')
            ->findOrFail($id);
    }
}
