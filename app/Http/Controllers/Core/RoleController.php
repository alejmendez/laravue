<?php

namespace App\Http\Controllers\Core;

// Control Base
use App\Http\Controllers\Controller as BaseController;

// Traits
use App\Traits\ApiResponse;
use App\Traits\RestControllerTrait;

// Request
use Illuminate\Http\Request;

// Modelos
use Spatie\Permission\Models\Role;

/**
 * Controlador de usuarios
 *
 * @category Controller
 * @package  App\Http\Controllers\Core
 * @author   Alejandro Méndez <alejmendez.87@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.html MIT
 * @link     http://url.com
 */
class RoleController extends BaseController
{
    use RestControllerTrait, ApiResponse;

    const MODEL = 'Spatie\Permission\Models\Role';
    const PERMISSION = 'role';

    protected $validationRules = [
        'username' => 'required',
        'name' => 'required',
        'password' => 'required'
    ];
}
