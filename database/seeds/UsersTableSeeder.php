<?php

use Illuminate\Database\Seeder;

use App\User;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User::truncate();
        User::flushEventListeners();

        $user = User::create([
            'name' => 'Administrador',
            'email' => 'admin@gmail.com',
            'password' => '1234qwer',
            'verified' => true,
            'super' => true,
            //'verification_token' => '',
        ]);

        $role = Role::findByName('admin');
        $user->assignRole($role);

        factory(User::class, 100)->create();
    }
}
